#!/bin/sh -e
UBOOT_VERSION="${CI_UBOOT:=2023.07}"
D="$(dirname $0)"
. "$D"/functions.sh
. "$D"/patch-uboot-silent.sh

init_build "$(dirname $D)"

# model a/b/zero
[ -n "$UBOOT_SILENT" ] && patch_config rpi_defconfig
build32 rpi_defconfig rpi1

# model zero w
[ -n "$UBOOT_SILENT" ] && patch_config rpi_0_w_defconfig
build32 rpi_0_w_defconfig rpi0_w

# model 2 b
[ -n "$UBOOT_SILENT" ] && patch_config rpi_2_defconfig
build32 rpi_2_defconfig rpi2

# model 3 (32 bit)
[ -n "$UBOOT_SILENT" ] && patch_config rpi_3_32b_defconfig
build32 rpi_3_32b_defconfig rpi3

# model 4 (32 bit)
[ -n "$UBOOT_SILENT" ] && patch_config rpi_4_32b_defconfig
build32 rpi_4_32b_defconfig rpi4

# 64 bit
[ -n "$UBOOT_SILENT" ] && patch_config rpi_arm64_defconfig
build64 rpi_arm64_defconfig rpi-64

make_artifact
