#!/bin/sh

init_build() {
  rm -rf "$1"/uboot
  mkdir -p "$1"/deploy "$1"/download "$1"/uboot
  CORES="-j$(($(nproc) + 2))"
  echo "using $CORES"

  UBOOT_ARCHIVE=${1}/download/u-boot-${UBOOT_VERSION}.tar.bz2
  if [ ! -e "${UBOOT_ARCHIVE}" ]; then
    wget -O ${UBOOT_ARCHIVE} http://ftp.denx.de/pub/u-boot/u-boot-${UBOOT_VERSION}.tar.bz2
  fi
  rm -rf u-boot-${UBOOT_VERSION}
  tar -xjf ${UBOOT_ARCHIVE}
  cd u-boot-${UBOOT_VERSION} || exit 1
}

copy_build() {
  cp u-boot.bin ../uboot/u-boot_$1.bin
  cp u-boot.map ../uboot/u-boot_$1.map
}

build32() {
  make CROSS_COMPILE=arm-linux-gnueabi- distclean
  make CROSS_COMPILE=arm-linux-gnueabi- $1
  make CROSS_COMPILE=arm-linux-gnueabi- "$CORES" u-boot.bin
  copy_build "$2"
}

build64() {
  make CROSS_COMPILE=aarch64-linux-gnu- distclean
  make CROSS_COMPILE=aarch64-linux-gnu- BL31="$3" "$1"
  make CROSS_COMPILE=aarch64-linux-gnu- BL31="$3" "$CORES"
  if [ -z "$3" ]; then
    copy_build "$2"
  fi
}

make_artifact() {
  cd ../uboot || exit 1
  [ -n "$UBOOT_SILENT" ] && POSTFIX="-silent"
  F="u-boot$1$POSTFIX-blob-$UBOOT_VERSION.tar.bz2"
  tar -cvjf ../deploy/"$F" ./*
  cd ../ || exit 1
  echo "$F" >>deploy/name
  echo "$UBOOT_VERSION" >deploy/version
}
